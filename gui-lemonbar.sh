#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/03/03 (Wed) 15:06:15 -03



  declare -A special
  special=(
              [^\[\[1~]=home
              [^\[\[2~]=ins
              [^\[\[3~]=del
              [^\[\[4~]=end
              [^\[\[5~]=pgup
              [^\[\[6~]=pgdn
                [^\[OP]=f1
                [^\[OQ]=f2
                [^\[OR]=f3
                [^\[OS]=f4
             [^\[\[15~]=f5
             [^\[\[17~]=f6
             [^\[\[18~]=f7
             [^\[\[19~]=f8
             [^\[\[20~]=f9
             [^\[\[21~]=f10
             [^\[\[23~]=f11
             [^\[\[24~]=f12
                  [^\[]=esc
                   [^I]=tab
                   [^?]=bs
                   [\$]=enter
                    [ ]=space

               [^\[\[A]=up
            [^\[\[1;2A]=s+up
            [^\[\[1;5A]=c+up

               [^\[\[B]=down
            [^\[\[1;2B]=s+down
            [^\[\[1;5B]=c+down

               [^\[\[C]=right
            [^\[\[1;2C]=s+right
            [^\[\[1;5C]=c+right

               [^\[\[D]=left
            [^\[\[1;2D]=s+left
            [^\[\[1;5D]=c+left

          )

  W=1080 ; H=1920 ; L=1920 ; T=0 # tamanho e canto esquerdo superior da tela na composição de monitores



  readkey()
  {
    local K1
    read -rsN1 K1 && while read -sN1 -t.001; do K1+=$REPLY; done
    K2=$(echo -n "$K1" | cat -A)
    [[ $K2 =~ \^([A-Z]) && ! ${special[$K2]} ]] && REPLY=ctrl+${BASH_REMATCH[1]} || REPLY=${special[$K2]:-$K1}
    [[ $1 == -d ]] && { echo -e "$K2\t$REPLY"; shift; }
    [[ $1 ]] && { eval $1=$REPLY; unset REPLY; }
  }



# while true; do readkey -d; done



  # screenruler
  # kmag
  # . <(xdotool getmouselocation --shell) # cooredenadas X,Y de $tlrect



  rdraw()
  {
    if [[ -v pid[$1] ]]
    then
      echo "Retângulo [$1] já posicionado com pid ${pid[$1]}"
    else
      declare -Ag pid
      exec 3>&2 2>-
      # <http://github.com/LemonBoy/bar>
      lemonbar -n selection -g ${2}x$3+$4+$5 -B#8800ff00 &
      pid[$1]=$!
      disown ${pid[$1]}
      exec 2>&3
    fi
  }



  sdraw()
  {
    [[ $1 == -k ]] && { rkill .; shift; }
    local     _w=${1:-$w}     _h=${2:-$h}   _l=${3:-$l} _t=${4:-$t}
    rdraw  t  $W              $_t           $L          $T
    rdraw  b  $W              $((H-_t-_h))  $L          $((_t+_h))
    rdraw  l  $((_l-L))       $_h           $L          $_t
    rdraw  r  $((W-_l+L-_w))  $_h           $((_l+_w))  $_t
  }



  rsel()
  {
    import /tmp/data.png
    data=$(identify /tmp/data.png)
    [[ -v IFo ]] || IFo=$IFS
    IFS=' +x'
    set $data
    IFS=$IFo
    w=$3   ; h=$4   ; l=$7   ; t=$8
    printf "Janela     W=%-4i  H=%-4i  L=%-4i  T=%-4i\n"  $W  $H  $L  $T
    printf "Retângulo  w=%-4i  h=%-4i  l=%-4i  t=%-4i\n"  $w  $h  $l  $t
    sdraw $w $h $l $t
  }



  smove()
  {
    i=${1:-1}
    while true
    do

      printf "Retângulo  w=%-4i  h=%-4i  l=%-4i  t=%-4i\n"  $w  $h  $l  $t

      readkey

      [[ $REPLY ==   up    ]] && sdraw -k $w $h $l        $((t-=i))
      [[ $REPLY ==   down  ]] && sdraw -k $w $h $l        $((t+=i))
      [[ $REPLY ==   left  ]] && sdraw -k $w $h $((l-=i)) $t
      [[ $REPLY ==   right ]] && sdraw -k $w $h $((l+=i)) $t

      [[ $REPLY == s+up    ]] && sdraw -k $w        $((h-=i)) $l $t
      [[ $REPLY == s+down  ]] && sdraw -k $w        $((h+=i)) $l $t
      [[ $REPLY == s+left  ]] && sdraw -k $((w-=i)) $h        $l $t
      [[ $REPLY == s+right ]] && sdraw -k $((w+=i)) $h        $l $t

      [[ $REPLY == esc     ]] && break

    done
  }



  rkill()
  {
    local i=$(tr \  \| <<< ${!pid[*]})
    case $1 in
    "@(${i:-+})" ) [[ -v pid[$1] ]] && kill -9 ${pid[$1]} ; unset pid[$1] ;;
    .            ) [[ -v pid[*]  ]] && kill -9 ${pid[*]}  ; unset pid     ;;
    *            ) if [[ -v pid[*] ]]
                   then
                     echo -e "\nUse: $FUNCNAME $i .\n\nPIDs:"
                     paste <(tr \  '\n' <<< ${!pid[*]}) <(tr \  '\n' <<< ${pid[*]}) | column -ts$'\t'
                     echo
                   else
                     echo "Nenhum retângulo a ser removido."
                   fi
    esac
  }



# EOF
