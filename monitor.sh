#!/bin/bash

  declare -A cards

#   card="Combined"
    card="Plantronics C310"

  notify="notify-send -i gtk-dialog-info -t 2000 Áudio"
  status=( 'em uso' 'livre' )
   cards=(
           [Plantronics C310]=alsa_output.usb-Plantronics_Plantronics_C310_53787CEF2CC38E48BA2AD22870AB59EC-00.analog-stereo
              [Áudio Interno]=alsa_output.pci-0000_00_1b.0.analog-stereo
                   [Combined]=combined
                   [Qualquer]=.
         )

  while true
  do
    pactl list short sinks | grep -Eiq ${cards[$card]}.+running
    exit=$?
    if (( last != exit ))
    then
      last=$exit
      play -qn -t alsa synth .1 sine 440
      $notify "A saída de áudio $card está ${status[last]}."
    fi
    sleep 1
  done

# EOF
