#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/03/02 (Tue) 00:59:33 -03

# <http://s2.dicas-l.com.br/ead/mod/page/view.php?id=987>
# <http://developer.gnome.org/pygtk/stable/pango-markup-language.html>
# <http://stackoverflow.com/questions/55348868/how-do-i-get-yad-for-gtk3-to-customize-button-font-background-and-foreground-co>



     theme=yad
      font='Data 70 LET'
      size=50
     state='Som detectado'
  themedir=$HOME/.themes/$theme/gtk-3.0

  mkdir -p $themedir

# ln -fs $PWD/gtk.css $themedir

  echo "window { background: #00ff00 }" > $themedir/gtk.css



  export    LC_ALL=C
# export GTK_DEBUG=interactive
  export GTK_THEME=$theme

# yad \
# --text "<span font_desc='$font' size='$((size*1000))'>$state</span>" \
# --no-buttons \
# --undecorated \
# --skip-taskbar \
# --selectable-labels \
# --on-top &

  w=200
  h=200
  l=$((1920+200))
  t=200

  yad \
  --text '' \
  --no-buttons \
  --undecorated \
  --skip-taskbar \
  --no-escape \
  --on-top \
  --geometry 200x200+$((1920+200))+200 \
  &

  sleep .1
  pid=$!
  wid=$(wmctrl -lp | grep -oP "^[^ ]+(?= .*$pid)")

  xprop -id $wid -f _NET_WM_WINDOW_OPACITY 32c -set _NET_WM_WINDOW_OPACITY 0xaaffffff

  sleep 3

  w=210
  h=210
  l=$((1920+200))
  t=200

  wmctrl -i -r $wid -e 0,$l,$t,$w,$h



# EOF
