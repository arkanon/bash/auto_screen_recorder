#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/03/06 (Sat) 23:08:33 -03
# 2021/03/05 (Fri) 02:02:30 -03
# 2021/03/03 (Wed) 15:06:15 -03
# 2021/03/03 (Wed) 05:04:15 -03
# 2021/03/02 (Tue) 00:58:36 -03

# ---- [DEPENDÊNCIAS] ----------------------------------------------------------------------------------------------------------------------------------- #

#   -- bash
#   -- bc
#   -- ffmpeg
#   -- kmag
#   -- pacmd
#   -- pactl
#   -- screenruler
#   -- wine
#   -- winepath
#   -- xclip
#   -- wmctrl
#   -- xdotool
#   -- yad
#   -- xte
#   -- lemonbar
#   -- import
#   -- identify
#   -- python3

# ---- [PARÂMETROS] ------------------------------------------------------------------------------------------------------------------------------------- #

  scriptp=$(readlink $0) # caminho físico do script
  scripte=${scriptp%.*}  # caminho físico SEM extensão

  declare -A props

  . $scripte-$USER.conf

# ---- [PROCEDIMENTO] ----------------------------------------------------------------------------------------------------------------------------------- #

  main()
  {

    mkdir -p $outd

    regedit $scriptd/toolbars.reg &> /dev/null

    # estas chamadas precisam ficar fora do bloco cuja saída é capturada pelo tee, caso contrário o prompt...
    fwid "$appbin" "$appname"     # ... é liberado apenas quando $appbin for finalizado
    echo -n "$(winepath -w "$filel" 2> /dev/null)" \
    | xclip -selection clipboard # ... não é liberado mesmo com o fechamento de $appbin (armazena o caminho windows do musx na área de transferência)

    {
      time \
      {
        debug ${debug[*]}
        pactl move-sink-input $stream $card             # [ 02 ] move o stream de áudio do aplicativo da saída atribuída para a especificada
        pacmd set-sink-mute $card 0                     #        unmuta o áudio
        sleep 1
        wmctrl -ir $fwid -b remove,$max                 #        restaura a janela
        xdo 0 windowsize $fwid ${wsize[*]}              # [ 04 ] redimensiona a janela
        xdo 0 windowmove $fwid ${wmove[*]}              # [ 03 ] move a janela para a posição desejada na composição de monitores
        xdo 0 key ctrl+o ctrl+v Return                  # [ 05 ] abre a janela de carga, cola o caminho windows do musx e carrega o arquivo
      # wmctrl -ir $fwid -b add,$max                    # [ 04 ] maximiza a janela (ignorado em favor do método de movimentação e redimensionamento da janela)
        xdo 2 key ctrl+0 type $zoom                     #        abre a janela de controle de zoom e escreve o valor do nível desejado
        xdo 0 key Return                                # [ 06 ] ajusta o nível de zoom
        xdo 0 key alt+v s                               # [ 07 ] entra em modo scroll view e
        xdo 0 key alt+v p                               # [ 07 ] entra em modo page view (para ajuste do posicionamento da página na janela)
        xdo 0 key alt+v h u                             #        esconde as réguas [booleano, mas não é uma config salva ao sair do finale]
      # xdo 0 key alt+v h l                             #        esconde os ícones de layout de página [booleano, mas é uma config salva ao sair do finale]
        xdo 0 key alt+d v                               # [ 08 ] seleciona saída de som por VST

      # . <(available)
        W=1080   ; H=1920    ; L=1920         ; T=21           ; rT=21 ; wH=1899
        w=$width ; h=$height ; l=${ltrect[0]} ; t=${ltrect[1]} ; rt=54
      # . <(rsel)
      # printf "Tela  W=%-4i  H=%-4i  L=%-4i  T=%-4i  rT=%-4i  wH=%-4i\n"  $W  $H  $L  $T  $rT  $wH
      # printf "Área  w=%-4i  h=%-4i  l=%-4i  t=%-4i  rt=%-4i\n"           $w  $h  $l  $t  $rt
      # sdraw $width $height ${ltrect[*]}               #        ofusca a tela onde não será gravada
        sdraw $w $h $l $t                               #        ofusca a tela onde não será gravada
        smove

  #     rec $fr $size ${ltrect[*]} $card "$outraw"      # [ 09 ] inicia a gravação do vídeo
  #     xdo 0 key alt+d p                               # [ 10 ] aciona o playback
  #     sleep $gracetime                                #        espera antes de começar a monitorar o silêncio, para passar peleas pausas do início

  #     case $stop_at in                                # [ 11 ] espera o momento do fim da gravação conforme o método indicado
  #       end  ) until end $card $silent; do :; done ;; #        monitora amostras de um segundo da saída de áudio testando se contém apenas silêncio
  #       time ) sleep $((forcetime-gracetime))      ;; #        espera a duração indicada para forçar o fim da gravação
  #     esac

  #     kill $fpid                                      # [ 12 ] finaliza a gravação
  #     xdo 0 key alt+d o                               # [ 13 ] interrompe o playback
  #     xdo 1 key alt+w w n                             # [ 14 ] fecha o musx sem salvar
  #     xdo 0 key alt+F4                                #        fecha o aplicativo
  #     [[ -e $themef ]] && lay $fr $themef $hshift $vshift "$outraw" "$outlay" || outlay="$outraw" # [ 15 ] sobrepõe o vídeo a um fundo na proporção final
  #     $publish && put "$outlay" $guser                # [ 16 ] publica no youtube
  #     echo
      }
    } |& tee "$log"

  }

# ---- [VARIÁVEIS AUXILIARES] --------------------------------------------------------------------------------------------------------------------------- #

  $profile

  appname="Finale"
   appbin="finale-26"
     mind=.2                  # delay mínimo entre as chamadas do xdotool

  scriptd=${scripte%/*}       # diretório do caminho físico do script
  scriptn=${scripte##*/}      # nome do script sem extensão
   themef=$scriptd/theme/$themen-${width}p.png
        : ${filel##*/}
    fpref=$outd/${_%.*}--$(alpha_date now)--${fr}fps--${width}p # prefixo do arquivo musx
   outraw=$fpref--${prop}.mp4 # vídeo gravado diretamente pela captura de tela
   outlay=$fpref--wide.mp4    # vídeo processado com o overlay
      log=$fpref.log
   height=$(bc <<< "$width*${props[$prop]}")
     size=${width}x${height}
     card=alsa_output.${cards[cardi]}.analog-stereo
      max=maximized_vert,maximized_horz

    debug=(
            scriptp
            zoom
            log
            filel
            fwid
            cardi
            cards[@]
            card
            stream
            width
            height
            ltrect[@]
          )

  export WINEPREFIX=/export/wine/apps/finale-26/user_prefixes/$USER
  export   WINEARCH=win64

# ---- [BIBLIOTECA] ------------------------------------------------------------------------------------------------------------------------------------- #

  . $scriptd/gui.sh

  alpha_date()
  {
    # now|<YYYY/MM/DD hh:mm>[:<ss>] [s]
    local l n a IFo A M D h s
    [[ ${*: -1} == s ]] && l=true || l=false
    (( $1 == now     )) && n=true || n=false
    $n && set $(date "+%Y/%m/%d %H:%M:%S")
    a=($*)
    ! $n && $l && unset a[${#a[*]}-1]
    IFo=$IFS
    IFS=:-/
    set ${a[*]}
    IFS=$IFo
    A=${1:2}
    M=$(letter $2)
    D=$3
    h=$([[ $4 ]] && letter $4)
    $l && s=.$6
    echo $A$M$D$h$5$s
  }

  debug  () { echo; for i; { echo DEBUG: $i=${!i}; } | column -ts=; echo; }
  letter () { local h; (( 10#$1 == 0 )) && echo z || { printf -v h %x $((10#$1+96)); printf \\x$h; }; }
  rec    () { ffmpeg -y -framerate $1 -draw_mouse 0 -f x11grab -video_size $2 -i :0.0+$3,$4 -f pulse -i $5.monitor -c:v h264_nvenc -qp 0 "$6" & fpid=$!; }
  lay    () { ffmpeg -y -framerate $1 -loop 1 -i $2 -i "$5" -filter_complex overlay=main_w-overlay_w+$3:main_h-overlay_h+$4:shortest=1 "$6"; }
  end    () { ffmpeg -ss 0 -t $2 -f pulse -i $1.monitor -af silencedetect=noise=-30dB:d=0.5 -f null - |& grep -q silence_start; }

  put()
  {
    : ${1##*/}
    : ${_%.*}
    local title=$(echo ${_//[_-]/ })
    $scriptd/upload.py \
      --privacyStatus "unlisted" \
      --category      "10" \
      --file          "$1" \
      --title         "$title" \
      --description   "" \
      --keywords      ""
    echo -e "\nGerencie seus vídeos em [http://studio.youtube.com/channel/$2/videos]"
  }

  xdo()
  {
    local wait
    [[ $1 == 0 ]] && wait=$mind || wait=$1
    shift
    sleep $wait
    xdotool windowactivate $fwid $*
  }

  fwid()
  {

    # [ 01 ] lança o aplicativo
    "$1"

    # quando o aplicativo terminar de abrir, captura a linha com o id da janela
    while ! fwid=$(
      wmctrl -l \
      | grep -E " $2($| - \[)"
    ); do sleep $mind; done

    # identifica o id da janela
    fwid=${fwid%% *}

    # identifica a saída atribuída ao stream de áudio do aplicativo
    while ! stream=$(
      LC_ALL=C
      pactl list sink-inputs \
      | grep -Ei "sink input|application\.name" \
      | grep -B1 "$2" \
      | grep -Eo [0-9]+ \
    ); do sleep $mind; done

  }

# ---- [EXECUÇÃO] --------------------------------------------------------------------------------------------------------------------------------------- #

  main "$@"

# ---- [EOF] -------------------------------------------------------------------------------------------------------------------------------------------- #
