#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2021/03/06 (Sat) 01:55:45 -03
# 2021/03/03 (Wed) 15:06:15 -03



  # while true; do readkey -d; done
  # screenruler
  # kmag
  # . <(xdotool getmouselocation --shell) # cooredenadas X,Y de $tlrect



  declare -A special
  special=(
              [^\[\[1~]=home
              [^\[\[2~]=ins
              [^\[\[3~]=del
              [^\[\[4~]=end
              [^\[\[5~]=pgup
              [^\[\[6~]=pgdn
                [^\[OP]=f1
                [^\[OQ]=f2
                [^\[OR]=f3
                [^\[OS]=f4
             [^\[\[15~]=f5
             [^\[\[17~]=f6
             [^\[\[18~]=f7
             [^\[\[19~]=f8
             [^\[\[20~]=f9
             [^\[\[21~]=f10
             [^\[\[23~]=f11
             [^\[\[24~]=f12
                  [^\[]=esc
                   [^I]=tab
                   [^?]=bs
                   [\$]=enter
                    [ ]=space

               [^\[\[A]=up
            [^\[\[1;2A]=s+up
            [^\[\[1;5A]=c+up

               [^\[\[B]=down
            [^\[\[1;2B]=s+down
            [^\[\[1;5B]=c+down

               [^\[\[C]=right
            [^\[\[1;2C]=s+right
            [^\[\[1;5C]=c+right

               [^\[\[D]=left
            [^\[\[1;2D]=s+left
            [^\[\[1;5D]=c+left

          )

     theme=yad
  themedir=$HOME/.themes/$theme/gtk-3.0
     color=blue
   opacity=.5
   opacity=$(bc <<< "obase=16;$opacity*256/1")


  shopt -s extglob

  mkdir -p $themedir
  echo "window{background:$color}" > $themedir/gtk.css



# main functions



  open()
  {
    close .
    . <(available)
    . <(rsel)
    printf "Tela  W=%-4i  H=%-4i  L=%-4i  T=%-4i  rT=%-4i  wH=%-4i\n"  $W  $H  $L  $T  $rT  $wH
    printf "Área  w=%-4i  h=%-4i  l=%-4i  t=%-4i  rt=%-4i\n"           $w  $h  $l  $t  $rt
    sdraw $w $h $l $t
  }



  smove()
  {
    local i=${1:-1}
    while true
    do
    # printf "Área  w=%-4i  h=%-4i  l=%-4i  t=%-4i\n"  $w  $h  $l  $t
      readkey
      case ${REPLY,,} in
          up    ) schange $w        $h        $l        $((t-=i)) ;;
          down  ) schange $w        $h        $l        $((t+=i)) ;;
          left  ) schange $w        $h        $((l-=i)) $t        ;;
          right ) schange $w        $h        $((l+=i)) $t        ;;
        s+up    ) schange $w        $((h+=i)) $l        $((t-=i)) ;;
        s+down  ) schange $w        $((h+=i)) $l        $t        ;;
        s+left  ) schange $((w+=i)) $h        $((l-=i)) $t        ;;
        s+right ) schange $((w+=i)) $h        $l        $t        ;;
        c+up    ) schange $w        $((h-=i)) $l        $t        ;;
        c+down  ) schange $w        $((h-=i)) $l        $((t+=i)) ;;
        c+left  ) schange $((w-=i)) $h        $l        $t        ;;
        c+right ) schange $((w-=i)) $h        $((l+=i)) $t        ;;
        esc|q   ) break ;;
      esac
    done
  }



  close()
  {
    local i=$(tr \  \| <<< ${!pid[*]})
    case $1 in
    @(${i:-+}) ) [[ -v pid[$1] ]] && kill -9 ${pid[$1]} ; unset pid[$1] wid[$1] ;;
    .          ) [[ -v pid[*]  ]] && kill -9 ${pid[*]}  ; unset pid     wid     ;;
    *          ) if [[ -v pid[*] ]]
                 then
                   echo -e "\nUse: $FUNCNAME $i .\n\nPIDs:"
                   paste <(tr \  '\n' <<< ${!pid[*]}) <(tr \  '\n' <<< ${pid[*]}) | column -ts$'\t'
                   echo
                 else
                   echo "Nenhum retângulo a ser removido."
                 fi
    esac
  }



# library



  available()
  {
    local tout=2 H
    ( xte "sleep $tout" "mouseclick 1" & ( xwininfo; killall xte ) ) &> /dev/null # espera um clique do mouse para abrir uma janela maximizada na tela clicada
    set $(area --fullscreen)
    H=$6
    set $(area --maximized)
    echo "W=$5; H=$H; wH=$6; L=$3; T=$4; rT=$((H-$6))"
  }



  area()
  {
    local pid wid
    exec 3>&2 2>&-
    yad --text '' --no-buttons --undecorated --skip-taskbar $1 &
    sleep .2
    pid=$!
    disown $pid
    wid=$(wmctrl -lp | grep -oP "^[^ ]+(?= .*$pid)")
    wmctrl -lG | grep $wid # wid ? L T W H ...
    wmctrl -ic $wid
    exec 2>&3
  }



  rsel()
  {
    import /tmp/data.png
    data=$(identify /tmp/data.png)
    [[ -v IFo ]] || IFo=$IFS
    IFS=' +x'
    set $data
    IFS=$IFo
    echo "w=$3; h=$4; l=$7; t=$8; rt=$(($8-T))"
  }



  sdraw()
  {
    local     _w=$1           _h=$2              _l=$3        _t=$4
    rdraw  t  $W              $((_t-T))          $L           $rT
    rdraw  r  $((W-_l-_w+L))  $_h                $((_l+_w))   $_t
    rdraw  b  $W              $((H-_t+T-rT-_h))  $L           $((_t+_h))
    rdraw  l  $((_l-L))       $_h                $L           $_t
  }



  rdraw()
  {
    (( $2<0 || $3<0 || $4<0 || $5<0 )) && { echo "Akgum valor inválido entre [ $* ]"; return 1; }
    [[ -v pid[$1] ]] && { echo "Área [$1] já posicionada com pid ${pid[$1]}" >&2; return 2; }
    declare -Ag pid wid
    exec 3>&2 2>&-
    GTK_THEME=$theme yad --text '' --no-buttons --undecorated --skip-taskbar --no-escape --on-top --geometry ${2}x$3+$4+$5 &
    sleep .2
    pid[$1]=$!
    wid[$1]=$(wmctrl -lp | grep -oP "^[^ ]+(?= .*${pid[$1]})")
    xprop -id ${wid[$1]} -f _NET_WM_WINDOW_OPACITY 32c -set _NET_WM_WINDOW_OPACITY 0x${opacity}ffffff
    disown ${pid[$1]}
    exec 2>&3
  }



  readkey()
  {
    local K1
    read -rsN1 K1 && while read -sN1 -t.001; do K1+=$REPLY; done
    K2=$(echo -n "$K1" | cat -A)
    [[ $K2 =~ \^([A-Z]) && ! ${special[$K2]} ]] && REPLY=ctrl+${BASH_REMATCH[1]} || REPLY=${special[$K2]:-$K1}
    [[ $1 == -d ]] && { echo -e "$K2\t$REPLY"; shift; }
    [[ $1 ]] && { eval $1=$REPLY; unset REPLY; }
  }



  schange()
  {
    declare -A w{t,r,b,l}
    local  w{t,r,b,l}
    local _w=$1              _h=$2                 _l=$3          _t=$4
    wt=(  [w]=$W             [h]=$((_t-T))         [l]=$L         [t]=$rT        )
    wl=(  [w]=$((_l-L))      [h]=$_h               [l]=$L         [t]=$_t        )
    wr=(  [w]=$((W-_l-_w+L)) [h]=$_h               [l]=$((_l+_w)) [t]=$_t        )
    wb=(  [w]=$W             [h]=$((H-_t+T-rT-_h)) [l]=$L         [t]=$((_t+_h)) )
    wmctrl -ir ${wid[t]} -e 0,${wt[l]},${wt[t]},${wt[w]},${wt[h]}
    wmctrl -ir ${wid[l]} -e 0,${wl[l]},${wl[t]},${wl[w]},${wl[h]}
    wmctrl -ir ${wid[r]} -e 0,${wr[l]},${wr[t]},${wr[w]},${wr[h]}
    wmctrl -ir ${wid[b]} -e 0,${wb[l]},${wb[t]},${wb[w]},${wb[h]}
  }



# EOF
