# auto_screen_recorder

Screen recorder autônomo para linha de comando e interface gráfica feito em shell script.

## Características

- gravação de uma área arbitrária da tela
- clique no botão play (se não houver tecla de atalho) de um editor de partitura
- interrupção da gravação assim que a saída de áudio deixar de ser usada
- mesclagem da saída com um template temático na proporção padrão de vídeo (|full)hd considerando a largura capturada
- publicação na internet

## TODO

### Linha de Comando
1. `ok` criar projeto público no gitlab
1. `ok` lançar o aplicativo
1. `ok` mover o stream de áudio do aplicativo da saída atribuída para a especificada
1. `ok` mover a janela para o segundo monitor
1. `ok` maximizar a janela
1. `ok` carregar o musx
1. `ok` finale com antialiasing nos elementos gráficos
1. `ok` ajustar o nível de zoom
1. `ok` entrar em scroll e depois page view para ajustar o posicionamento da página na janela
1. `ok` selecionar saída de som por VST
1. `ok` iniciar a gravação de vídeo  720p
1. `ok` iniciar a gravação de vídeo 1080p
1. `ok` acionar o playback
1. `ok` esperar o momento do fim da gravação conforme o método indicado
1. `ok` finalizar a gravação
1. `ok` interromper o playback
1. `ok` fechar o musx sem salvar
1. `ok` sobrepor o vídeo a um fundo na proporção correta
1. `ok` esconder réguas
1. `ok` medir o tempo de execução
1. `ok` salvar uma cópia da saída em log
1. `ok` separar os parâmetros específicos de cada proporção em perfis de gravação
1. `ok` se o finale já estiver aberto, aproveitar a janela aberta e não abrir novamente
1. `ok` publicar no youtube
1. `ok` definir parâmetros por arquivo de configuração

---

1. `--` [vídeos entrando bloqueados quando enviados pela api]
1. `--` ao iniciar, identificar se há algum processo preso do finale (se houver, pode impedir de colar o caminho do musx)
1. `--` apresentar estatísticas ao final do procedimento
1. `--` associar à uma opção de menu de contexto do PCManFM
1. `--` converter sob demanda o svg do tema para png no tamanho necessário e guardar como cache
1. `--` criar controle para o caso de erro na execução do finale
1. `--` definir parâmetros por linha de comando
1. `--` detectar se os ícones de layout de página estão visíveis e escondê-los
1. `--` discutir a ordenação quebrada por usar a letra z para a hora 0
1. `--` documentar a criação de projeto, aplicativo e credencial
1. `--` entrar fechando as barras de ferramenta (configuração salva no registro)
1. `--` erro na identificação do caminho quando executado fora do path
1. `--` executar em uma sessão virtual VNC para gravação (Full|Ultra)HD em monitores pequenos
1. `--` gravar múltiplos musx em loop
1. `--` helper gráfico para identificar o canto superior esquerdo do retângulo de gravação
1. `--` incluir dinâmicamente o nome da música no cabeçalho
1. `--` incluir uma miniatura personalizada na publicação
1. `--` listar sources de áudio disponíveis
1. `--` mudar cores da página e do cursor
1. `--` opção de apenas gravar ou apenas publicar
1. `--` publicar com node.js pela interface web para não ficar limitado pela quota da api
1. `--` publicação no youtube apresenta tremor do cursor e do overlay
1. `--` versão overlay do vídeo usar mesmos parâmetros de compactação

### Interface Gráfica



```
# EOF
```
