#!/usr/bin/python3

# Arkanon <arkanon@lsd.org.br>
# 2021/03/06 (Sat) 23:06:58 -03
# 2021/03/05 (Fri) 15:39:33 -03
# 2021/03/05 (Fri) 01:33:50 -03

#   # <https://support.google.com/youtube/answer/7300965?hl=pt-BR> [Vídeos bloqueados como privados]
#   # <https://console.cloud.google.com/iam-admin/quotas>
#   # <https://console.cloud.google.com/iam-admin/quotas/details;servicem=youtube.googleapis.com;metricm=youtube.googleapis.com%2Fdefault;limitIdm=1%2Fd%2F%7Bproject%7D>
#   # <https://developers.google.com/youtube/v3/getting-started#quota>
#   # <https://stackoverflow.com/questions/15568405/youtube-api-limitations>
#   # <https://developers.google.com/youtube/v3/determine_quota_cost>
#   # <https://www.youtube.com/watch?v=JNEJPffln1k> [Understanding Quota in v3]
#
#   # <https://developers.google.com/youtube/v3/guides/uploading_a_video>
#   # <https://developers.google.com/drive/api/v3/quickstart/python>
#   # <https://github.com/googleapis/google-api-python-client>
#   # <https://github.com/youtube/api-samples>
#
#   sudo pip3 install testresources google-api-python-client google_auth_oauthlib
    # na primaira vez que buscar upload_video.py:
# # wget https://raw.githubusercontent.com/youtube/api-samples/master/python/upload_video.py
# # chmod +x upload.py
# # sudo apt install 2to3
# # 2to3 upload.py -w
# # sed -ri "1s/python[0-9]?$/&3/; /CLIENT_SECRETS_FILE/s/'client_secret.json'/os.environ.get('SECRETS')/" upload.py
#
#   # <https://gist.github.com/dgp/1b24bf2961521bd75d6c> [category id list]
#   # <https://console.developers.google.com>
#   # 1. create an account on the Google Developers Console
#   # 2. register a new app there
#   # 3. enable the Youtube API (APIs & Auth -> APIs)
#   # 4. create Client ID (APIs & Auth -> Credentials), select 'Other'
#   # 5. download the secrets file clicking on the download icon



import os

# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the {{ Google Cloud Console }} at {{ https://cloud.google.com/console }}.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
CLIENT_SECRETS_FILE = os.environ.get('secrets')
TOKEN_PICKLE_FILE   = os.environ.get('pickle')



import argparse
import http.client
import httplib2
import pickle
import random
import time

import google.oauth2.credentials
import google_auth_oauthlib.flow
from   googleapiclient.discovery      import build
from   googleapiclient.errors         import HttpError
from   googleapiclient.http           import MediaFileUpload
from   google_auth_oauthlib.flow      import InstalledAppFlow
from   google.auth.transport.requests import Request



httplib2.RETRIES       =  1 # Explicitly tell the underlying HTTP transport library not to retry, since we are handling retry logic ourselves
MAX_RETRIES            = 10 # Maximum number of times to retry before giving up

RETRIABLE_EXCEPTIONS   = (  # Always retry when these exceptions are raised
                           httplib2.HttpLib2Error,
                           IOError,
                           http.client.NotConnected,
                           http.client.IncompleteRead,
                           http.client.ImproperConnectionState,
                           http.client.CannotSendRequest,
                           http.client.CannotSendHeader,
                           http.client.ResponseNotReady,
                           http.client.BadStatusLine
                         )

RETRIABLE_STATUS_CODES = [  # Always retry when an apiclient.errors.HttpError with one of these status codes is raised
                           500, 502, 503, 504
                         ]

SCOPES                 = [  # To allow an application upload files to the authenticated user's YouTube channel, but no other type of access
                           'https://www.googleapis.com/auth/youtube.upload'
                         ]

API_SERVICE_NAME       = 'youtube'
API_VERSION            = 'v3'
VALID_PRIVACY_STATUSES = ('public', 'private', 'unlisted')



# Authorize the request and store authorization credentials.
def get_authenticated_service():

  creds = None
  # The file token.pickle stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  if os.path.exists(TOKEN_PICKLE_FILE):
    with open(TOKEN_PICKLE_FILE, 'rb') as token:
      creds = pickle.load(token)
  # If there are no (valid) credentials available, let the user log in.
  if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
      creds.refresh(Request())
    else:
      flow  = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
      creds = flow.run_local_server(port=0)
    # Save the credentials for the next run
    with open(TOKEN_PICKLE_FILE, 'wb') as token:
      pickle.dump(creds, token)
    os.chmod(TOKEN_PICKLE_FILE, 0o600)

  return build(API_SERVICE_NAME, API_VERSION, credentials=creds)



def initialize_upload(youtube, options):
  tags = None
  if options.keywords:
    tags = options.keywords.split(',')

  body=dict(
    snippet=dict(
      title=options.title,
      description=options.description,
      tags=tags,
      categoryId=options.category
    ),
    status=dict(
      privacyStatus=options.privacyStatus
    )
  )

  # Call the API's videos.insert method to create and upload the video.
  insert_request = youtube.videos().insert(
    part=','.join(list(body.keys())),
    body=body,
    # The chunksize parameter specifies the size of each chunk of data, in
    # bytes, that will be uploaded at a time. Set a higher value for
    # reliable connections as fewer chunks lead to faster uploads. Set a lower
    # value for better recovery on less reliable connections.
    #
    # Setting 'chunksize' equal to -1 in the code below means that the entire
    # file will be uploaded in a single HTTP request. (If the upload fails,
    # it will still be retried where it left off.) This is usually a best
    # practice, but if you're using Python older than 2.6 or if you're
    # running on App Engine, you should set the chunksize to something like
    # 1024 * 1024 (1 megabyte).
    media_body=MediaFileUpload(options.file, chunksize=-1, resumable=True)
  )

  resumable_upload(insert_request)



# This method implements an exponential backoff strategy to resume a
# failed upload.
def resumable_upload(request):
  response = None
  error = None
  retry = 0
  while response is None:
    try:
      print('Uploading file...')
      status, response = request.next_chunk()
      if response is not None:
        if 'id' in response:
          print('Video id "%s" was successfully uploaded.' % response['id'])
        else:
          exit('The upload failed with an unexpected response: %s' % response)
    except HttpError as e:
      if e.resp.status in RETRIABLE_STATUS_CODES:
        error = 'A retriable HTTP error %d occurred:\n%s' % (e.resp.status,
                                                             e.content)
      else:
        raise
    except RETRIABLE_EXCEPTIONS as e:
      error = 'A retriable error occurred: %s' % e

    if error is not None:
      print(error)
      retry += 1
      if retry > MAX_RETRIES:
        exit('No longer attempting to retry.')

      max_sleep = 2 ** retry
      sleep_seconds = random.random() * max_sleep
      print('Sleeping %f seconds and then retrying...' % sleep_seconds)
      time.sleep(sleep_seconds)



if __name__ == '__main__':

  parser = argparse.ArgumentParser()
  parser.add_argument('--file', required=True, help='Video file to upload')
  parser.add_argument('--title', help='Video title', default='Test Title')
  parser.add_argument('--description', help='Video description',
    default='Test Description')
  parser.add_argument('--category', default='22',
    help='Numeric video category. ' +
      'See https://developers.google.com/youtube/v3/docs/videoCategories/list')
  parser.add_argument('--keywords', help='Video keywords, comma separated',
    default='')
  parser.add_argument('--privacyStatus', choices=VALID_PRIVACY_STATUSES,
    default='private', help='Video privacy status.')
  args = parser.parse_args()

  youtube = get_authenticated_service()

  try:
    initialize_upload(youtube, args)
  except HttpError as e:
    print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))



# EOF
